package com.example.myapplication.models

data class Task(
    val description: String,
    val isUrgent: Boolean,
    val isDone: Boolean = false,
    var id: Long = 0
)
