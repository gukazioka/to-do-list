package com.example.myapplication.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.R
import com.example.myapplication.data.TaskRequest
import com.example.myapplication.data.TaskSingleton
import com.example.myapplication.models.Task
import com.example.myapplication.ui.components.TaskItemView
import com.example.myapplication.ui.themes.AlmostWhite01

@Composable
fun MainScreenView(taskRequest: TaskRequest, modifier: Modifier = Modifier) {
    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.primary
            ) {
                Text(
                    text = stringResource(id = R.string.app_name),
                    fontSize = 20.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(3.dp),
                    textAlign = TextAlign.Start
                )
            }
        },
        bottomBar = {
            TaskFormView(taskRequest)
        }
    ) {
        innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)
            .fillMaxWidth()) {
                TaskListView(taskRequest)
            }
        }
}

@Composable
fun TaskListView(taskRequest: TaskRequest, modifier: Modifier = Modifier) {
    LazyColumn(
        Modifier.padding(
            horizontal = 3.dp
        )
    ) {
        items(TaskSingleton.getTasks()) { task ->
            TaskItemView(task = task, taskRequest)
        }
    }
}

@Composable
fun TaskFormView(taskRequest: TaskRequest, modifier: Modifier = Modifier) {
    val checkedState = remember { mutableStateOf(false) }
    val taskName = remember { mutableStateOf(TextFieldValue("")) }

    Column(){
        Row(){
            Switch(
                checked = checkedState.value,
                onCheckedChange = {checkedState.value = it}
            )
            Text(
                text = "Urgent",
                style = MaterialTheme.typography.body2
            )
        }
        Row( modifier = Modifier
            .fillMaxWidth()
        ) {
            TextField(modifier = Modifier
                .fillMaxWidth(),
                value = taskName.value,
                onValueChange = {taskName.value = it}
            )
        }
        Row(){
            Button(modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
                onClick = {
                    taskRequest.createNewTask(Task(
                        description = taskName.value.text,
                        isUrgent = checkedState.value
                    ))
                    taskName.value = TextFieldValue("")
                }
            ) {
                Text("Send")
            }
        }
    }
}