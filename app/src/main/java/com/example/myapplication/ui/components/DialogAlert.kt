package com.example.myapplication.ui.components

import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import com.example.myapplication.data.TaskRequest
import com.example.myapplication.models.Task

@Composable
fun DialogAlert(task: Task, taskRequest: TaskRequest, openDialog: MutableState<Boolean>) {
    if (openDialog.value) {
        AlertDialog(
            onDismissRequest = { openDialog.value = false },
            title = { Text(text = "Excluir task?",
                color = MaterialTheme.colors.onPrimary)
            },
            text = { Text(
                text = "Deseja apagar a task com id ${task.id}",
                color = MaterialTheme.colors.onPrimary)
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        taskRequest.deleteTasks(task.id)
                    }
                ) {
                    Text(text = "Sim", color = MaterialTheme.colors.onPrimary)
                }
            },
            dismissButton = {
                TextButton(onClick = { openDialog.value = false }) {
                    Text(text = "Não", color = MaterialTheme.colors.onPrimary)
                }
            }
        )
    }
}
