package com.example.myapplication.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myapplication.data.TaskRequest
import com.example.myapplication.models.Task
import com.example.myapplication.ui.screens.MainScreenView
import com.example.myapplication.ui.themes.AlmostBlack02
import com.example.myapplication.ui.themes.AlmostWhite03
import com.example.myapplication.ui.themes.ToDoListAppTheme


@Composable
fun TaskItemView(task: Task, taskRequest: TaskRequest, modifier: Modifier = Modifier) {

    val openDialog = remember { mutableStateOf(false) }
    val maxLine = remember { mutableStateOf(1) }

    Card(modifier = Modifier
            .fillMaxWidth(),
            backgroundColor = MaterialTheme.colors.primaryVariant
    ) {
        DialogAlert(task = task, taskRequest = taskRequest, openDialog = openDialog)
        Row(
            Modifier
                .height(intrinsicSize = IntrinsicSize.Min)
                .pointerInput(Unit) {
                    detectTapGestures(
                        onLongPress = {
                            openDialog.value = true
                        },
                        onTap = {
                            if (maxLine.value == 1) maxLine.value = 30 else maxLine.value = 1
                        }
                    )
                },
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            TaskUrgency(task.isUrgent)
            TaskInfo(task.description, maxLine.value, Modifier
                .fillMaxWidth(0.6f)
                .weight(1f)
            )
            TaskCheckBox(task, taskRequest,
                Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            )
        }
    }
}

@Composable
fun TaskUrgency(isUrgent: Boolean, modifier: Modifier = Modifier) {
    Box(modifier = Modifier
        .width(5.dp)
        .fillMaxHeight()
        .background(if (isUrgent) Color.Red else Color.Green)
    )
}

@Composable
fun TaskInfo(description: String, maxLine: Int, modifier: Modifier = Modifier) {
    Text(
        text = description,
        maxLines = maxLine,
        modifier = Modifier
            .fillMaxWidth(0.6f ),
        fontSize = 14.sp,
        style = MaterialTheme.typography.h3,
        color = MaterialTheme.colors.onPrimary,
        overflow = TextOverflow.Ellipsis
    )
}

@Composable
fun TaskCheckBox(task: Task, taskRequest: TaskRequest, modifier: Modifier = Modifier) {
    val checkedState = remember { mutableStateOf(task.isDone) }
    Checkbox(
        checked = checkedState.value,
        onCheckedChange = {
            checkedState.value = it
            taskRequest.updateTasks(checkedState.value, task.id)
        }
    )
}

//@Preview
//@Composable
//fun PreviewTaskItemView() {
//    ToDoListAppTheme() {
//        TaskItemView(Task("asasassaassasasassaassa", false, false))
//    }
//}
//
