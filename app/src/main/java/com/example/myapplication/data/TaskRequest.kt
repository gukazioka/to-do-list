package com.example.myapplication.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.example.myapplication.models.Task
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

class TaskRequest(context: Context) {
    private val queue = Volley.newRequestQueue(context)

    companion object {
        private const val URL = "http://10.0.2.2:8080"
        private const val GET_TASKS = "/tasks"
        private const val CREATE_TASKS = "/tasks/new"
        private const val UPDATE_TASKS = "/tasks/done"
        private const val DELETE_TASKS = "/tasks/del"
    }

    fun startTasksRequest() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                tasksRequest()
                handler.postDelayed(this, 5000)
            }
        })
    }

    fun tasksRequest() {
        Log.i("Doing a request.", "Doing a request")
        val jsonRequest = JsonArrayRequest(
            Request.Method.GET,
            URL + GET_TASKS,
            null,
            { response ->
                val taskList = JSONArrayToTaskList(response)
                TaskSingleton.updateTaskList(taskList)
            },
            { error ->
                Log.e("TaskError", "Task request error: $error")
            }
        )
        this.queue.add(jsonRequest)
    }

    fun createNewTask(task: Task) {
        val jsonObject = JSONObject()
        jsonObject.put("description", task.description)
        jsonObject.put("is_urgent", task.isUrgent)
        jsonObject.put("is_done", task.isDone)

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST,
            URL + CREATE_TASKS,
            jsonObject,
            { response ->
                Log.i("ReqOk", "Tudo certo meu patrão ${response}")
            },
            { error ->
                Log.e("ReqCreateError", "Connection error. ${error.toString()}")
            }
        )
        this.queue.add(jsonObjectRequest)
    }

    fun updateTasks(isDone: Boolean, taskId: Number) {
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.PUT,
            URL + UPDATE_TASKS + "/" + isDone + "/" + taskId,
            null,
            { response ->
                Log.i("ReqOk", "Tudo certo meu patrão ${response}")
            },
            { error ->
                Log.e("ReqCreateError", "Connection error. ${error.toString()}")
            }
        )
        this.queue.add(jsonObjectRequest)
    }

    fun deleteTasks(taskId: Number) {
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.DELETE,
            URL + DELETE_TASKS + "/" + taskId,
            null,
            { response ->
                Log.i("ReqOk", "Tudo certo meu patrão ${response}")
            },
            { error ->
                Log.e("ReqCreateError", "Connection error. ${error.toString()}")
            }
        )
        this.queue.add(jsonObjectRequest)
    }

    fun JSONArrayToTaskList(jsonArray: JSONArray): ArrayList<Task> {
        val taskList = ArrayList<Task>()
        for(i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val task = Task(
                jsonObject.getString("description"),
                jsonObject.getBoolean("is_urgent"),
                jsonObject.getBoolean("is_done"),
                jsonObject.getLong("id")
            )
            taskList.add(task)
        }
        return taskList
    }
}