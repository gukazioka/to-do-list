package com.example.myapplication.data

import androidx.compose.runtime.mutableStateListOf
import com.example.myapplication.models.Task

object TaskSingleton {
    private val tasks = mutableStateListOf<Task>()

    fun updateTaskList(tasks: ArrayList<Task>){
        this.tasks.clear()
        this.tasks.addAll(tasks)
    }

    fun getTasks(): List<Task> {
        return this.tasks
    }
}