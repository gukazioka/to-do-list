package com.example.myapplication

import android.os.Build
import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.TaskRequest
import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
import com.example.myapplication.ui.screens.MainScreenView
import com.example.myapplication.ui.themes.ToDoListAppTheme

class MainActivity : ComponentActivity() {
    private lateinit var taskRequest: TaskRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.taskRequest = TaskRequest(this)
        this.taskRequest.startTasksRequest()
        setContent {
            ToDoListAppTheme() {
                this.SetupUIConfigs()
                MainScreenView(this.taskRequest)
            }
        }
    }

    @Composable
    private fun SetupUIConfigs() {
        if (MaterialTheme.colors.isLight) {
            window.decorView
                .windowInsetsController?.setSystemBarsAppearance(
                    APPEARANCE_LIGHT_STATUS_BARS, APPEARANCE_LIGHT_STATUS_BARS
                )
        } else {
            window.decorView
                .windowInsetsController?.setSystemBarsAppearance(
                    0, APPEARANCE_LIGHT_STATUS_BARS
                )
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }
}